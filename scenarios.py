from pate_framework import Pate
import logging
# import pandas as pd
# import numpy as np
# from sklearn.preprocessing import LabelEncoder
import psycopg2
from pandas.io.sql import PandasSQL


class Scenarios:

    def __init__(self, num_teacher=1, num_student=1, percentage_teachers=None, percentage_student=None,
                 percentage_storage=None, teacher_division=None, student_division=None, dataset=None,
                 label=None):

        logging.basicConfig(level=logging.DEBUG)
        logging.debug("creation of scenario")

        # int
        self.num_teachers = num_teacher
        # int
        self.num_student = num_student
        # float
        self.percentage_teachers = percentage_teachers
        # float
        self.percentage_storage = percentage_storage
        # [float] (len = num_teachers)
        self.teacher_division = teacher_division
        # [float] (len = num_student)
        self.student_division = student_division
        # dataset (no base format for now )
        self.dataset = dataset
        # str
        self.label = label
        # str ( classification, regression )
        self.model_finding = None
        # creating object from Teacher_Student_Modelization class
        self.pate = Pate(num_teacher=self.num_teachers, num_student=self.num_student,
                         dataset=self.dataset, label=self.label,
                         model_finding=self.model_finding,
                         percentage_storage=self.percentage_storage,
                         percentage_teachers=self.percentage_teachers, dp_option=True)

        self.pate.launch_teacher()

    def script_1(self, student_id):

        logging.info(msg="launching script 1")

        # launch queries for students (all students for now, to be modified later on )
        self.pate.launch_student(mode=1, student_id=student_id)

    def script_2(self, student_id):

        logging.info(msg="launching script 2")

        # launch queries for students (all students for now, to be modified later on )
        self.pate.launch_student(mode=2, student_id=student_id)

    def script_3(self, student_id):

        logging.info(msg="launching script 3")

        # launch queries for students (all students for now, to be modified later on )
        self.pate.launch_student(mode=3, student_id=student_id)


# works with kaggle healthcare dataset stroke data csv : https://www.kaggle.com/fedesoriano/stroke-prediction-dataset
"""
dataset_ = pd.read_csv('./data/datasets/healthcare-dataset-stroke-data.csv')
dataset_.drop(columns='id', axis=1, inplace=True)
bmi_mean = dataset_['bmi'].mean()
dataset_['bmi'].replace(np.nan, bmi_mean, inplace=True)
print(dataset_['stroke'].shape)
cat_col = dataset_.select_dtypes(['object']).columns
print(cat_col)
label_encode = LabelEncoder()
# initializing an object of class LabelEncoder
for i in dataset_.columns:
    dataset_[i] = label_encode.fit_transform(dataset_[i])
# print(dataset_.stroke.shape)
print(dataset_.stroke.nunique())
test = Scenarios(num_teacher=1, num_student=1, percentage_teachers=0.9, percentage_storage=0.5,
                  dataset=dataset_, label='stroke')
"""
test = Scenarios(num_teacher=2, num_student=3, percentage_teachers=0.3, percentage_storage=0.9)
test.script_1(list(test.pate.dict_student.keys())[0])

test.script_2(list(test.pate.dict_student.keys())[0])
test.script_3(list(test.pate.dict_student.keys())[0])
