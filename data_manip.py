"""
this file is used to manipulate data used in pate_framework.py
"""
import logging
import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.model_selection import train_test_split
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.preprocessing.sequence import pad_sequences
from dp import *


class data_manipulation:

    def __init__(self, pate):
        """
        :param pate:
        """

        logging.basicConfig(level=logging.DEBUG)
        self.pate = pate

    @staticmethod
    def load_cifar_10():
        """
        - import data from cifar_10
        - convert images into float32 bounded [0,1]
        - one hot encode labels
        - assemble images and labels
        """
        logging.info(msg="forming data")

        (x_train, y_train), (x_test, y_test) = tf.keras.datasets.cifar10.load_data()
        logging.info("cifar dataset has been downloaded")

        # convert from integers to floats
        x_train_float = x_train.astype('float32')
        x_test_float = x_test.astype('float32')
        # normalize to range 0-1
        x_train_norm = x_train_float / 255.0
        x_test_norm = x_test_float / 255.0

        # turn labels into categories array
        y_train_cat = to_categorical(y_train)
        y_test_cat = to_categorical(y_test)

        # put all train test values into one
        data = np.append(x_train_norm, x_test_norm, axis=0)
        label = np.append(y_train_cat, y_test_cat, axis=0)
        data_list = list(data)
        label_list = list(label)
        return data_list, label_list

    def create_dataframe(self, x=None, y=None):
        """
        if data does not come directly from from_data:
            :parameter (feature X, label Y)
            :return dataset containing X and Y

        if data comes directly from form_data:
            - takes X and Y from cifar_10
            :return dataset
        """
        logging.info(msg="creating dataframe")

        dataset = None
        dataset_transpose = None

        # if the user has not submitted a dataframe, uses cifar_10 by default
        if self.pate.aut_cifar is True:

            # if there are x and y in parameter declared to form a dataframe from queries for instance
            if (x is not None) and (y is not None):
                logging.info("creating cifar_10 dataset from student queries to teachers")
                x = list(x)

                dataset = pd.DataFrame([x, y])
                dataset_transpose = dataset.transpose()
                dataset_transpose.columns = ['image', 'label']
                dataset_transpose.insert(0, 'id', range(0, 0 + len(dataset_transpose)))
                dataset_transpose = dataset_transpose.dropna()
                logging.info("dataset has been created")
                return dataset_transpose

            else:
                # if self.Dataset is not declared in __init__ parameters
                if self.pate.dataset is None:
                    logging.info("Downloading Cifar_10 dataset")

                    self.pate.columns = ['image', 'label']
                    # creation of dataframe from form_data --> cifar_10
                    dataset = pd.DataFrame(self.load_cifar_10())
                    dataset_transpose = dataset.transpose()
                    dataset_transpose.columns = self.pate.columns
                    dataset_transpose.insert(0, 'id', range(0, 0 + len(dataset_transpose)))

                    self.pate.dataset = dataset_transpose
                    self.pate.label = 'label'
                    logging.info("dataset has been created")
                    return dataset_transpose

        # if the user submitted a dataframe
        else:

            # if the inputs x and y are not empty in the case of teacher queries
            if (x is not None) and (y is not None):
                logging.info("creating dataset from queries to teacher")

                y = list(y)
                x = list(x)
                columns = np.delete(self.pate.columns.values, np.argwhere(self.pate.columns.values == self.pate.label))

                # columns = self.columns.remove(self.label)
                dataset = pd.DataFrame(x, columns=columns)
                dataset[self.pate.label] = y
                logging.info("dataset has been created")
                return dataset

            # if x and y are not declared in parameters
            else:
                logging.info("loading/formatting dataset inputted")

                self.pate.columns = self.pate.dataset.columns
                dataset = self.pate.dataset
                if 'id' not in self.pate.columns:
                    dataset.insert(0, 'id', range(0, 0 + len(self.pate.dataset)))

                logging.info("dataset properly loaded/formatted")
                return dataset

    def distribute_data(self, dict_teacher=None, dict_student=None):

        dataset = self.create_dataframe()

        # creating Size_Storage depending on Size_Storage parameter
        if self.pate.percentage_storage is not None:
            # creating a fraction of the dataset
            fraction_of_dataset = dataset.sample(frac=self.pate.percentage_storage, random_state=1)
            logging.info("percentage of storage is {}".format(self.pate.percentage_storage))
            fraction_of_dataset = fraction_of_dataset.reset_index()
            # deleting samples of the dataset which have the same index as samples of fraction_data
            list_id = list(filter(lambda x: x in dataset['id'], fraction_of_dataset['id']))

            dataset = dataset.drop(
                dataset[
                    dataset.id.isin(list_id)
                ].index
            )

            logging.info("storage data created")
            fraction_of_dataset.drop(fraction_of_dataset.columns[[0, 1]], axis=1, inplace=True)

            self.pate.storage_data = fraction_of_dataset
            # .drop(fraction_of_dataset.columns[[0, 1]], axis=1, inplace=True)

        # initiating the sizes for each teachers and students
        samples_per_teacher = int(len(dataset) * (self.pate.percentage_teachers * 100) / (100 * self.pate.num_teacher))
        samples_per_student = int(len(dataset) *
                                  ((1 - self.pate.percentage_teachers) * 100) / (100 * self.pate.num_student))

        logging.info("samples per teacher is {}".format(samples_per_teacher))
        logging.info("samples per student is {}".format(samples_per_student))

        # same technique for teachers and students as storage_data
        for teacher_id in self.pate.dict_teacher.keys():
            fraction_of_dataset = dataset.sample(n=samples_per_teacher, random_state=1)
            fraction_of_dataset = fraction_of_dataset.reset_index()
            # teacher_dataset[teacher_id] = fraction_of_dataset

            list_id = list(filter(lambda x: x in dataset['id'], fraction_of_dataset['id']))
            dataset = dataset.drop(
                dataset[
                    dataset.id.isin(list_id)
                ].index
            )

            logging.info("{} data created".format(teacher_id))
            fraction_of_dataset.drop('id', axis=1)
            self.pate.teacher_data[teacher_id] = fraction_of_dataset

        for student_id in self.pate.dict_student.keys():
            fraction_of_dataset = dataset.sample(n=samples_per_student, random_state=1)
            fraction_of_dataset = fraction_of_dataset.reset_index()
            # student_dataset[student_id] = fraction_of_dataset

            list_id = list(filter(lambda x: x in dataset['id'], fraction_of_dataset['id']))
            dataset = dataset.drop(
                dataset[
                    dataset.id.isin(list_id)
                ].index
            )
            fraction_of_dataset.drop('id', axis=1)

            logging.info("{} data created".format(student_id))
            self.pate.student_data[student_id] = fraction_of_dataset

        return self.pate.teacher_data, self.pate.student_data

    def split_train_test(self, data, user_id):
        """
        gives the format of data before being split w/ train_test_split in order to be inserted in Neural Network

        :parameter user_id:
        :parameter data--> dataset
        :returns X_train,X_test,y_train,y_test
        """
        # if the user has not submitted dataframe
        if self.pate.aut_cifar is True:

            logging.info(msg="shaping and splitting data of {}".format(user_id))
            # formatting image and labels to then separate in order to fit the model
            arr = []
            # formatting image and labels to then separate in order to fit the model
            for i in data.image.values:
                arr.append(i)

            x = np.array(arr)
            y = pad_sequences(data.label)

            x_train, x_test, y_train, y_test = train_test_split(x, y, train_size=0.7, random_state=42)

            logging.info("data has been separated")
            return x_train, x_test, y_train, y_test

        # if the user has submitted a dataframe
        else:
            logging.info(msg="shaping and splitting data of {}".format(user_id))

            # formatting of features and labels in order to fit the model
            dataset = self.Dataset.drop('id', axis=1)
            x = dataset.drop(self.label, axis=1).values
            y = dataset[self.label].values

            x_train, x_test, y_train, y_test = train_test_split(x, y, train_size=0.7, random_state=42)

            logging.info("{} data has been separated".format(user_id))
            print(x_train.shape)
            self.student_data[user_id] = [x_train, x_test, y_train, y_test]
            return x_train, x_test, y_train, y_test

    # compute
    def query_teachers(self, student_id, dict_teacher, queries=None, maximum_queries=5):
        """
        parameters :
            - Teacher dictionary
            - query or in this case images of size 32,32,3

        created a dataframe containing the images and Teacher responses
        returns : teacher_votes
        """
        if queries is None:
            queries = self.pate.storage_data["image"]

        logging.info(msg="{} launches {} queries to teachers")
        print(type(self.pate.dataset[self.pate.label][0]))
        teacher_votes = []
        for query in queries:
            result = []
            if np.issubdtype(type(self.pate.dataset[self.pate.label][0]), np.integer):
                increments = np.zeros(self.pate.dataset[self.pate.label].nunique())
            else:
                increments = np.zeros(self.pate.dataset[self.pate.label][0].shape[0])

            query = np.array([query])
            # TODO : update in order to call model from model_manipulator or from pate_framework
            for key_ in self.pate.dict_teacher.keys():
                model = self.pate.dict_model[key_].model
                result.append(np.argmax(model.predict(query)))

            for i in range(len(increments)):
                for value in result:
                    if i == value:
                        increments[i] += 1
            maximum_queries -= 1

            teacher_votes.append(list(increments))
            if maximum_queries == 0:
                logging.info("NUMBER OF QUERIES EXCEEDED LIMIT")
                break

        logging.info("queries have ceased for {}".format(student_id))
        return self.create_dataframe(queries, teacher_votes)

    # TODO: must return an array containing the statistics from student data [ mean, variance, standard-deviation ]
    def get_statistics_from_student_data(self, student_id):
        data = self.pate.student_data[student_id]

        labels = []
        for y in data['label']:
            labels.append(np.argmax(y))
        labels = np.array(labels)

        mean = np.mean(labels)
        std = np.std(labels)
        var = np.var(labels)

        self.pate.student_data_stats[student_id] = [mean, std, var]
        logging.info("collected stats from {} data".format(student_id))