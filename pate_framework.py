import logging

import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, classification_report
from tensorflow.keras import Sequential
from tensorflow.keras.metrics import Accuracy
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Dense, Flatten
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.losses import categorical_crossentropy
from dp import *
from codecarbon import EmissionsTracker
# compute
# storage - no metrics
# network - no metrics

from data_manip import data_manipulation
from ml_manip import ModelManipulator


class Pate:
    # TODO trouver nom plus clair pour storage

    def __init__(self, num_teacher=1, num_student=1, dataset=None, label=None, model_finding=None,
                 percentage_teachers=0.5, percentage_storage=0.5, dp_option=False, dp_parameters=[]):
        """
        :param num_teacher: number of teachers initiated
        :param num_student: number of student initiated
        :param dataset: dataset input by user
        :param label: label input by user
        :param model_finding: type of model
        :param percentage_teachers: percentage of data stored in teachers
        :param percentage_storage: percentage of data stored in storage
        :param dp_option: usage of differential privacy or not
        :param dp_parameters: list of differential privacy parameters to be used to instantiate dp_engine,
        it should contain respectively 5 values:
        [sensitivity,target_epsilon, target_delta, noise_count, noise_generation_mechanism, threshold]

        """

        logging.basicConfig(level=logging.DEBUG)

        logging.info(msg="Creation of modelization class")

        # number of teachers initiated {int}
        assert (
            num_teacher > 0
        ), "There must be at least 1 teacher"
        self.num_teacher = num_teacher
        # number of students initiated {int}
        assert(
            num_student > 0
        ), "there must be at least 1 student"
        self.num_student = num_student
        # if user has input a dataset then the dataset is in the variable
        self.dataset = dataset
        # if the user has not input a dataset, the cifar will be the dataset used
        self.aut_cifar = self.dataset is None
        # the label of the dataset
        self.label = label
        # contains columns of dataset
        self.columns = None

        # percentage of dataset to teachers
        assert (
            type(percentage_teachers) is float and percentage_teachers > 0.0
        ), "teacher(s)'s percentage must be float and over 0%"
        self.percentage_teachers = percentage_teachers
        # percentage of dataset to storage
        assert(
            type(percentage_storage) is float and percentage_teachers > 0.0
        ), "storage's percentage must be float and over 0%"
        self.percentage_storage = percentage_storage

        # limit of queries student can ask teachers {int}
        self.query_limit = 100
        # to know if DP is being used or not
        assert (
            type(dp_option) is bool
        ), "dp_option must be True or False"
        self.dp_option = dp_option

        if dp_option:
            assert (
                len(dp_parameters) == 5 or 0
            ), "Incorrect number of values for dp parameters: 5 if dp_option is set to true otherwise it should be 0"
            self.dp_parameters = dp_parameters
            if self.dp_parameters:
                self.dp_engine = DpEngine(dp_parameters[0],
                                          dp_parameters[1],
                                          dp_parameters[2],
                                          dp_parameters[3],
                                          dp_parameters[4])
            else:
                self.dp_engine = DpEngine()

        # compositions of teachers and students
        # at the end, it should be {Student_i : [user_id, scenario, dp_option, accuracy]}
        self.dict_teacher = None
        self.dict_student = None

        # contains dataset from all students {dict} --> {Teacher_i : dataset}
        self.student_data = dict()
        # contains dataset from all teachers {dict} --> {Teacher_i : dataset}
        self.teacher_data = dict()
        # storage data (optional) {float}
        self.storage_data = None

        # dict of models
        self.dict_model = dict()
        # dict of student_data
        self.student_data_stats = dict()

        # type of dataset (image / classification / regression ) {int?}
        self.model_finding = model_finding

        # get values of teacher predictions
        self.teacher_predictions = dict()
        # get values of teacher predictions after procedure (argmax or differential privacy w/ argmax)
        self.teacher_answers_after_procedure = {}

        # creates teachers and students --> updates
        self.create_teacher_student(num_teacher, num_student)

        logging.info("creation of data controller")
        self.data_controller = data_manipulation(self)
        logging.info("data controller created")

        self.teacher_data, self.student_data = self.data_controller.distribute_data(self.dict_teacher.copy(),
                                                                                    self.dict_student.copy())
        logging.info("creation of model manipulator")
        self.model_manipulator = ModelManipulator(self, self.data_controller)
        logging.info("model manipulator created")

    # compute
    def create_teacher_student(self, num_teachers, num_students):
        """
        :param num_teachers: number of teachers
        :param num_students: number of students
        :return: dict_teacher & dict_student
        """
        logging.info(msg="creating teacher(s) & student(s)")

        teacher_title = 'Teacher_'
        student_title = 'Student_'
        dict_teacher = {}
        dict_student = {}

        # creation of students from n_teacher
        for i in range(num_teachers):
            dict_teacher[teacher_title + str(i + 1)] = None

        # creation of students from n_student
        for i in range(num_students):
            dict_student[student_title + str(i + 1)] = None

        self.dict_teacher = dict_teacher
        self.dict_student = dict_student
        logging.info("{} have/has been created".format(dict_teacher.keys()))
        logging.info("{} have/has been created".format(dict_student.keys()))
        return dict_teacher, dict_student

    # compute
    def fit_model(self, member_data, member_type, user_id):
        """
        :param member_data:
        :param member_type:
        :param user_id:
        :return: [model,X_train,X_test,y_train,y_test]

        first the data is formed and split
        then we receive model depending on  type_user
        the we fit the model with the data received
        """

        # data is split
        x_train, x_test, y_train, y_test = member_data
        # define model
        model = self.model_manipulator.define_model(member_type, user_id)
        # fit model
        logging.info(msg="fitting {} model".format(user_id))

        history = model.fit(x_train, y_train, epochs=1, batch_size=100, validation_data=(x_test, y_test),
                            callbacks=EarlyStopping(monitor='val_loss', patience=2))
        self.dict_model[user_id] = history

        return model

    # orchestration
    def launch_teacher(self):

        """
                :parameter
                    - percentage_Teachers : size of Teachers dataset (float)
                    - Size_Storage : size of storage dataset (float)

                runs run_harness for all teachers
                updates dict_Teachers

                :returns : dict_teacher
                """

        tracker = EmissionsTracker(project_name="training_teachers")
        tracker.start()
        logging.info("inside tracker")

        for i in self.teacher_data.keys():
            logging.info("launch for {}".format(i))
            self.fit_model(self.data_controller.split_train_test(self.teacher_data[i], i), 'teacher', i)

        tracker.stop()

    def launch_student(self, mode, student_id, data=None):
        """

        :param mode: what is the scenario queried
        :param student_id: which student is being applied the scenario
        :param data: which data is going to be used
        :return: updated dict_student
        """

        logging.info(msg="Run scenario {} for {}".format(mode, student_id))
        # self.dict_student[student_id] = \
        self.launch_scenario(student_id, mode, self.storage_data)
        accuracy_from_prediction = self.predict_from_storage(student_id)

        self.dict_student[student_id] = [student_id, mode, self.dp_option, accuracy_from_prediction]

        logging.info("get statistics from {} data".format(student_id))
        self.data_controller.get_statistics_from_student_data(student_id)
        # create function in order to make student model predict

        logging.info("scenario {} for {} is complete".format(mode, student_id))

    def launch_scenario(self, student_id, mode=1, dataset=None):
        """

        :param student_id: onto which student
        :param mode: which mode
        :param dataset: which dataset
        :return: updated dict_student

        recreates a model for the student
        3 versions :
            1) use existing student model in order to fit it with queries only

            2) use new model to fit it with student data & queries

            3) use new model to fit it with queries only

        updates student dict values
        """

        tracker = EmissionsTracker(project_name="training_student_mode_{}".format(mode))
        tracker.start()

        # if the mode queried is 1, then the student data is split in order to fit a student type model
        if mode == 1:
            logging.info("split data and fit model for {}".format(student_id))

            # TODO : we need to edit this line in order to make the return of fit_model only modify the ml part not the
            #  dict_student
            # self.dict_student = [student_id, function_below, mode, self.dp_option]
            # we then insert model prediction data (inside function launch_student)
            # self.dict_student[student_id] = \
            self.fit_model(self.data_controller.split_train_test(
                self.student_data[student_id], student_id), 'student', student_id
            )

        elif mode == 2:

            # calling query_teachers in order to get Teacher's predictions
            logging.info("send queries to teachers")
            self.teacher_predictions[student_id] = self.data_controller.query_teachers(student_id,
                                                                                       self.dict_teacher)

            # create copy of teachers' answers before applying differential privacy
            self.teacher_answers_after_procedure = self.teacher_predictions.copy()

            # check if we are using Differential Privacy
            if self.dp_option:
                # for all queries votes i.e [0, 0, 2, 3, 0, 0] --> 2 and 3 votes for index 2 and 3 respectfully
                for vote_index in range(self.teacher_answers_after_procedure[student_id].shape[0]):
                    # initialize np.array of zeros getting length of size of labels
                    returned_dp_teacher_prediction = np.zeros(10)

                    # retrieve vote array from teacher_predictions i.e [0,0,1,3,1,0,0]
                    vote_vector = \
                        self.teacher_answers_after_procedure[student_id][self.label][vote_index]
                    """
                    add Differential Privacy to the vote_vector
                    it returns the index of the value having the most teacher's votes
                    set the index of the label having the most votes to 1
                    we then get for example [0,0,0,1,0,0,0,0,0,0]
                    """
                    returned_dp_teacher_prediction[self.dp_engine.original_pate_aggregator(vote_vector)] = 1
                    logging.info("DP has been applied onto teachers' response")
                    # teacher_predictions now has the DP values of
                    self.teacher_answers_after_procedure[student_id][self.label][vote_index] = \
                        returned_dp_teacher_prediction

            if not self.aut_cifar:
                # applying function to labels in order for them to get initial value format
                self.teacher_answers_after_procedure[student_id][self.data_controller.label] = \
                    self.teacher_answers_after_procedure[student_id][self.data_controller.label].apply\
                        (lambda x: np.argmax(x))

            # the teacher's labels are added to the student's data
            logging.info("adding teachers' response to student_data")
            self.student_data[student_id] = self.student_data[student_id].append(
                self.teacher_predictions[student_id].reset_index()
            )

            logging.info("split data and fit model for {}".format(student_id))
            self.fit_model(self.data_controller.split_train_test(
                self.student_data[student_id], student_id),'student', student_id
            )

        else:

            logging.info("split data and fit model for {}".format(student_id))
            self.fit_model(self.data_controller.split_train_test(
                self.student_data[student_id], student_id), 'student', student_id
            )

            logging.info("send queries to teachers")
            self.teacher_predictions[student_id] = self.data_controller.query_teachers(student_id, self.dict_teacher)

            # create copy of teachers' answers before applying differential privacy
            self.teacher_answers_after_procedure = self.teacher_predictions.copy()

            # TODO :factorise pour éviter les conditions dp_option
            if not self.dp_option:
                # applying function to labels in order for them to get initial value format
                logging.info("apply differential privacy")

                for index, row in self.teacher_answers_after_procedure[student_id].iterrows():
                    max_vote_index = row[self.data_controller.label].index(max(row[self.data_controller.label]))
                    teacher_prediction_vector = np.zeros(10)
                    teacher_prediction_vector[max_vote_index] = 1
                    self.model_manipulator.take_gradient_step(self.dict_student[student_id][0], row["image"],
                                                              teacher_prediction_vector)
                logging.info("gradient tape has been applied onto {}".format(self.model_manipulator[student_id].name))
            else:
                for index, row in self.teacher_answers_after_procedure[student_id].iterrows():
                    max_vote_index = self.dp_engine.original_pate_aggregator(row[self.label])
                    teacher_prediction_vector = np.zeros(10)
                    teacher_prediction_vector[max_vote_index] = 1
                    self.model_manipulator.take_gradient_step(self.dict_model[student_id].model, row["image"],
                                                              teacher_prediction_vector)
                logging.info("gradient tape has been applied onto {}".format(self.dict_model[student_id].model.name))

        tracker.stop()

        return self.dict_student[student_id]

    def predict_from_storage(self, user_id):
        """

        :param user_id:
        :return: accuracy of model from predictions on storage data
        """
        logging.info("{} predicting from storage data".format(self.dict_model[user_id].model.name))

        y_predicted = []
        y_true = []
        model = self.dict_model[user_id].model

        for X in self.storage_data['image']:
            x_np = np.expand_dims(X, axis=0)
            y_predicted.append(np.argmax(model(x_np)[0]))

        for y in self.storage_data["label"]:
            y_true.append(np.argmax(y))

        accuracy = Accuracy()
        accuracy.update_state(y_true, y_predicted)
        accuracy = accuracy.result().numpy()
        # logging.info(confusion_matrix(y_predicted, y_true))

        return accuracy
